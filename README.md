# Install
Please config .env (db connection)
## 1
```shell
composer install
```
# 2
```shell
php bin/console make:migration
```
# 3
```shell
php bin/console doctrine:migrations:migrate  
```
# 4
```shell
symfony server:start
```
