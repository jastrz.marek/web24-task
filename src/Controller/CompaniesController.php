<?php
declare(strict_types=1);

namespace App\Controller;

use App\Services\Companies\GetCompanies;
use App\Services\Companies\HrOperations;
use App\Services\Companies\ModifyCompanies;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CompaniesController extends AbstractController
{
	public function __construct(private readonly ManagerRegistry    $registry,
								private readonly ValidatorInterface $validator)
	{
	}

	#[Route('/company/{id}', methods: 'GET')]
	public function getCompanyById(int $id): JsonResponse
	{
		$company = new GetCompanies($this->registry);
		$company->getById($id);

		return new JsonResponse($company->getResults(), $company->getCode());
	}

	#[Route('/company/search/{nip}', methods: 'GET')]
	public function getCompanyByNip(string $nip): JsonResponse
	{
		$company = new GetCompanies($this->registry);
		$company->getByNip($nip);

		return new JsonResponse($company->getResults(), $company->getCode());
	}

	#[Route('/company/employees/{companyId}', methods: 'GET')]
	public function getCompanyEmployees(int $companyId): JsonResponse
	{
		$company = new GetCompanies($this->registry);
		$company->getEmployees($companyId);

		return new JsonResponse($company->getResults(), $company->getCode());
	}

	#[Route('/company', methods: 'POST')]
	public function createCompany(Request $request): JsonResponse
	{
		$requestContent = json_decode($request->getContent(), true);
		$company = new ModifyCompanies($this->registry, $this->validator);
		$company->create($requestContent);

		return new JsonResponse($company->getResults(), $company->getCode());
	}

	#[Route('/company/{id}', methods: ['PUT', 'PATCH'])]
	public function updateCompany(int $id, Request $request): JsonResponse
	{
		$requestContent = json_decode($request->getContent(), true);
		$company = new ModifyCompanies($this->registry, $this->validator);
		$company->update($id, $requestContent);

		return new JsonResponse($company->getResults(), $company->getCode());
	}

	#[Route('/company/{id}', methods: 'DELETE')]
	public function deleteCompany(int $id): JsonResponse
	{
		$company = new ModifyCompanies($this->registry, $this->validator);
		$company->delete($id);

		return new JsonResponse($company->getResults(), $company->getCode());
	}

	#[Route('/company/hire/{companyId}/{employeeId}', methods: ['PATCH'])]
	public function hireEmployee(int $companyId, int $employeeId): JsonResponse
	{
		$hr = new HrOperations($this->registry, $this->validator);
		$hr->hireEmployee($companyId, $employeeId);

		return new JsonResponse($hr->getResults(), $hr->getCode());
	}

	#[Route('/company/hire/{companyId}', methods: ['PUT'])]
	public function hireNewEmployee(Request $request, int $companyId): JsonResponse
	{
		$requestContent = json_decode($request->getContent(), true);
		$hr = new HrOperations($this->registry, $this->validator);
		$hr->hireNewEmployee($companyId, $requestContent);

		return new JsonResponse($hr->getResults(), $hr->getCode());
	}

	#[Route('/company/fire/{companyId}/{employeeId}', methods: ['PATCH'])]
	public function fireEmployee(int $companyId, int $employeeId): JsonResponse
	{
		$hr = new HrOperations($this->registry, $this->validator);
		$hr->fireEmployee($companyId, $employeeId);

		return new JsonResponse($hr->getResults(), $hr->getCode());
	}
}
