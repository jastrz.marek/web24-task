<?php
declare(strict_types=1);

namespace App\Controller;

use App\Services\Employees\GetEmployees;
use App\Services\Employees\ModifyEmployees;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EmployeesController extends AbstractController
{
	public function __construct(private ValidatorInterface $validator,
								private ManagerRegistry    $registry)
	{
	}

	#[Route('/employee/{id}', methods: 'GET')]
	public function getEmployee(int $id): JsonResponse
	{
		$employee = new GetEmployees($this->registry);
		$employee->getById($id);

		return new JsonResponse($employee->getResults(), $employee->getCode());
	}

	#[Route('/employee/search/{email}', methods: 'GET')]
	public function getEmployeeByEmail(string $email): JsonResponse
	{
		$employee = new GetEmployees($this->registry);
		$employee->getByEmail($email);

		return new JsonResponse($employee->getResults(), $employee->getCode());
	}

	#[Route('/employee/company/{id}', methods: 'GET')]
	public function getEmployeeCompany(int $id): JsonResponse
	{
		$employee = new GetEmployees($this->registry);
		$employee->getCompany($id);

		return new JsonResponse($employee->getResults(), $employee->getCode());
	}

	#[Route('/employee', methods: 'POST')]
	public function createEmployee(Request $request): JsonResponse
	{
		$requestContent = json_decode($request->getContent(), true);
		$employee = new ModifyEmployees($this->registry, $this->validator);
		$employee->create($requestContent);

		return new JsonResponse($employee->getResults(), $employee->getCode());
	}

	#[Route('/employee/{id}', methods: ['PUT', 'PATCH'])]
	public function updateEmployee(int $id, Request $request): JsonResponse
	{
		$requestContent = json_decode($request->getContent(), true);
		$employee = new ModifyEmployees($this->registry, $this->validator);
		$employee->update($id, $requestContent);

		return new JsonResponse($employee->getResults(), $employee->getCode());
	}

	#[Route('/employee/{id}', methods: 'DELETE')]
	public function deleteEmployee(int $id): JsonResponse
	{
		$employee = new ModifyEmployees($this->registry, $this->validator);
		$employee->delete($id);

		return new JsonResponse($employee->getResults(), $employee->getCode());
	}
}