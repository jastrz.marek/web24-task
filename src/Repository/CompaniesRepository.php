<?php

namespace App\Repository;

use App\Entity\Companies;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Companies>
 *
 * @method Companies|null find($id, $lockMode = null, $lockVersion = null)
 * @method Companies|null findOneBy(array $criteria, array $orderBy = null)
 * @method Companies[]    findAll()
 * @method Companies[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompaniesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Companies::class);
    }

    public function save(Companies $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Companies $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

	public function getCompanyByNip(string $nip): array
	{
		$query = $this->createQueryBuilder('company')
			->select('company')
			->where('company.nip = :nip')
			->orderBy('company.id','DESC')
			->setParameter(':nip', $nip)
			->getQuery();

			return $query->getArrayResult();
	}

	public function getCompanyById(int $id): array
	{
		$query = $this->createQueryBuilder('company')
			->select('company')
			->where('company.id = :id')
			->orderBy('company.id','DESC')
			->setParameter(':id', $id)
			->getQuery();

		return $query->getArrayResult();
	}

	public function getCompanyEmployes(int $id): array
	{
		$query = $this->createQueryBuilder('company')
			->select('employee')
			->leftJoin('App:Employees', 'employee', \Doctrine\ORM\Query\Expr\Join::WITH, 'employee.fkIdCompany = company.id')
			->where('company.id = :id')
			->orderBy('company.id','DESC')
			->setParameter(':id', $id)
			->getQuery();

		return $query->getArrayResult();
	}
}
