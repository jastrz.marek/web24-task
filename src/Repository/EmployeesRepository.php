<?php

namespace App\Repository;

use App\Entity\Employees;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Employees>
 *
 * @method Employees|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employees|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employees[]    findAll()
 * @method Employees[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeesRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, Employees::class);
	}

	public function save(Employees $entity, bool $flush = false): void
	{
		$this->getEntityManager()->persist($entity);

		if ($flush) {
			$this->getEntityManager()->flush();
		}
	}

	public function remove(Employees $entity, bool $flush = false): void
	{
		$this->getEntityManager()->remove($entity);

		if ($flush) {
			$this->getEntityManager()->flush();
		}
	}

	public function getEmployeeByEmail(string $email): array
	{
		$query = $this->createQueryBuilder('employees')
			->select('employees')
			->where('employees.email = :email')
			->orderBy('employees.id','DESC')
			->setParameter(':email', $email)
			->getQuery();

		return $query->getArrayResult();
	}

	public function getEmployeeById(int $id): array
	{
		$query = $this->createQueryBuilder('employees')
			->select('employees')
			->where('employees.id = :id')
			->orderBy('employees.id','DESC')
			->setParameter(':id', $id)
			->getQuery();

		return $query->getArrayResult();
	}


	public function getEmployeeWithCompany(int $id): array
	{
		$query = $this->createQueryBuilder('employees')
			->select('companies')
			->leftJoin('App:Companies', 'companies', \Doctrine\ORM\Query\Expr\Join::WITH, 'companies.id = employees.fkIdCompany')
			->where('employees.id = :id')
			->orderBy('employees.id','DESC')
			->setParameter(':id', $id)
			->getQuery();

		return $query->getArrayResult();
	}
}
