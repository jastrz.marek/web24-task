<?php
declare(strict_types=1);
namespace App\Utils\Validation;

use function Symfony\Component\Translation\t;

class Validate
{
	public function __construct()
	{
	}

	final public static function email(mixed $email): bool
	{
		if(!is_string($email)){
			return false;
		}
		$pattern = '/(?:[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/';
		preg_match($pattern, $email, $matches);

		return match (true){
			count($matches) === 1 => true,
			default =>false
		};
	}

	final public static function nip(string $nip): bool
	{
		preg_match('/\d{10}/', $nip, $matches);

		return (bool) $matches;
	}
}