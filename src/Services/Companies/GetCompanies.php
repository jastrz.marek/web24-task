<?php
declare(strict_types=1);

namespace App\Services\Companies;

use App\Repository\CompaniesRepository;
use App\Services\AbstractService;
use App\Utils\Validation\Validate;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GetCompanies extends AbstractService
{
	public function getById(int $id): void
	{
		$this->results = (new CompaniesRepository($this->registry))->getCompanyById($id)[0] ?? [];
		$this->code = match ((bool)$this->results) {
			true => Response::HTTP_OK,
			false => Response::HTTP_NO_CONTENT
		};
	}

	public function getByNip(string $nip): void
	{
		if (!Validate::nip($nip)) {
			$this->code = Response::HTTP_BAD_REQUEST;
			$this->results[] = 'nip wrong length';
			return;
		}

		$this->results = (new CompaniesRepository($this->registry))->getCompanyByNip($nip)[0] ?? [];
		$this->code = match ((bool)$this->results) {
			true => Response::HTTP_OK,
			false => Response::HTTP_NO_CONTENT
		};
	}

	final public function getEmployees(int $id): void
	{
		$results = (new CompaniesRepository($this->registry))->getCompanyEmployes($id);
		if (!$results) {
			$this->code = Response::HTTP_NO_CONTENT;
			return;
		}

		$this->code = Response::HTTP_OK;
		$this->results = $results;
	}




}