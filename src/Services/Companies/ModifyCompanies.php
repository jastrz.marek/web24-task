<?php
declare(strict_types=1);

namespace App\Services\Companies;

use App\Entity\Companies;
use App\Repository\CompaniesRepository;
use App\Services\AbstractService;
use Symfony\Component\HttpFoundation\Response;

class ModifyCompanies extends AbstractService
{
	public function update(int $id, array|null $request): void
	{
		$company = $this->registry->getRepository(Companies::class)->find($id);
		if (!is_array($request)) {
			$this->code = Response::HTTP_BAD_REQUEST;
			return;
		}
		if (!$company) {
			$this->create($request);
			return;
		}

		$company = $this->setCompanyData($request, $company);
		$errors = $this->validator->validate($company);
		if (count($errors) > 0) {
			$this->code = Response::HTTP_BAD_REQUEST;
			$this->setErrorResults($errors);
			return;
		}

		$this->setResults($company);
		$this->code = Response::HTTP_OK;
	}

	public function create(array|null $request): void
	{
		if (!is_array($request)) {
			$this->code = Response::HTTP_BAD_REQUEST;
			return;
		}
		$company = $this->setCompanyData($request);
		$errors = $this->validator->validate($company);
		if (count($errors) > 0) {
			$this->code = Response::HTTP_BAD_REQUEST;
			$this->setErrorResults($errors);
			return;
		}

		$this->setResults($company);
		$this->code = Response::HTTP_OK;
	}

	private function setCompanyData(array $request, Companies|null $company = null): Companies
	{
		$company = $company ?? new Companies();
		!isset($request['companyName']) ?: $company->setCompanyName($request['companyName']);
		!isset($request['nip']) ?: $company->setNip($request['nip']);
		!isset($request['street']) ?: $company->setStreet($request['street']);
		!isset($request['buildingNumber']) ?: $company->setBuildingNumber($request['buildingNumber']);
		!isset($request['zipCode']) ?: $company->setZipCode($request['zipCode'] ?? '');
		!isset($request['flatNumber']) ?: $company->setFlatNumber($request['flatNumber']);
		!isset($request['city']) ?: $company->setCity($request['city']);

		return $company;
	}

	private function setResults(Companies $company): void
	{
		(new CompaniesRepository($this->registry))->save($company, true);
		$newEmployee = new GetCompanies($this->registry);
		$newEmployee->getById($company->getId());

		$this->results = $newEmployee->getResults();
	}

	public function delete(int $id): void
	{
		$company = $this->registry->getRepository(Companies::class)->find($id);
		if (!$company) {
			$this->code = Response::HTTP_BAD_REQUEST;
			return;
		}

		(new CompaniesRepository($this->registry))->remove($company, true);
		$this->code = Response::HTTP_OK;
	}
}