<?php
declare(strict_types=1);

namespace App\Services\Companies;

use App\Entity\Companies;
use App\Entity\Employees;
use App\Repository\CompaniesRepository;
use App\Repository\EmployeesRepository;
use App\Services\AbstractService;
use App\Services\Employees\ModifyEmployees;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class HrOperations extends AbstractService
{
	private EmployeesRepository $employeeRepository;
	private CompaniesRepository $companiesRepository;

	public function __construct(ManagerRegistry $registry, ?ValidatorInterface $validator = null)
	{
		parent::__construct($registry, $validator);
		$this->employeeRepository = new EmployeesRepository($registry);
		$this->companiesRepository = new CompaniesRepository($registry);
	}

	public function hireEmployee(int $companyId, $employeeId): void
	{
		$company = $this->registry->getRepository(Companies::class)->find($companyId);
		$employee = $this->registry->getRepository(Employees::class)->find($employeeId);
		if (!$company || !$employee) {
			$this->code = Response::HTTP_NO_CONTENT;
			return;
		}

		$employee->setFkIdCompany($company);
		$this->employeeRepository->save($employee, true);
		$result = new GetCompanies($this->registry);
		$result->getEmployees($companyId);

		$this->results = $result->getResults();
		$this->code = Response::HTTP_OK;
	}

	public function fireEmployee(int $companyId, int $employeeId): void
	{
		$company = $this->registry->getRepository(Companies::class)->find($companyId);
		$employee = $this->registry->getRepository(Employees::class)
			->findOneBy(['id' => $employeeId, 'fkIdCompany' => $company]);
		if (!$employee) {
			$this->code = Response::HTTP_NO_CONTENT;
			return;
		}

		$employee->setFkIdCompany(null);
		$this->employeeRepository->save($employee, true);
		$this->results = $this->companiesRepository->getCompanyEmployes($companyId);
		$this->code = Response::HTTP_OK;
	}

	public function hireNewEmployee(int $companyId, array $request)
	{
		$company = $this->registry->getRepository(Companies::class)->find($companyId);
		if (!$company) {
			$this->code = Response::HTTP_NO_CONTENT;
		}
		$request['fkIdCompany'] = $company;
		$employee = new ModifyEmployees($this->registry, $this->validator);
		$employee->create($request);

		$this->code = $employee->getCode();
		$this->results = $this->companiesRepository->getCompanyEmployes($companyId);
	}
}