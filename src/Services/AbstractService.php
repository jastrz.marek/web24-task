<?php

namespace App\Services;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AbstractService
{
	protected array $results = [];
	protected int $code;

	public function __construct(protected readonly ManagerRegistry    $registry,
								protected readonly ValidatorInterface|null $validator =null)
	{
	}

	final public function getResults(): array
	{
		return $this->results;
	}

	final public function getCode(): int
	{
		return $this->code;
	}

	final protected function setErrorResults(ConstraintViolationListInterface $errors): void
	{
		foreach ($errors as $error) {
			$this->results[] = $error->getMessage();
		}
	}
}