<?php
declare(strict_types=1);

namespace App\Services\Employees;

use App\Repository\EmployeesRepository;
use App\Utils\Validation\Validate;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;

class GetEmployees
{
	private array $results = [];
	private int $code;

	public function __construct(private readonly ManagerRegistry $registry)
	{
	}

	final public function getResults(): array
	{
		return $this->results;
	}


	final public function getCode(): int
	{
		return $this->code;
	}

	final public function getById(int $id): void
	{
		$this->results = (new EmployeesRepository($this->registry))->getEmployeeById($id)[0] ?? [];
		$this->code = match ((bool)$this->results) {
			true => Response::HTTP_OK,
			false => Response::HTTP_NO_CONTENT
		};
	}

	final public function getCompany(int $id): void
	{
		$results = (new EmployeesRepository($this->registry))->getEmployeeWithCompany($id);
		if (!$results) {
			$this->code = Response::HTTP_NO_CONTENT;
			return;
		}

		$this->code = Response::HTTP_OK;
		$this->results = $results;
	}

	public function getByEmail(string $email): void
	{
		if (!Validate::email($email)) {
			$this->code = Response::HTTP_BAD_REQUEST;
			return;
		}

		$this->results = (new EmployeesRepository($this->registry))->getEmployeeByEmail($email);
		$this->code = match ((bool)$this->results) {
			true => Response::HTTP_OK,
			false => Response::HTTP_NO_CONTENT
		};
	}
}