<?php
declare(strict_types=1);

namespace App\Services\Employees;

use App\Entity\Employees;
use App\Repository\EmployeesRepository;
use App\Services\AbstractService;
use Symfony\Component\HttpFoundation\Response;

class ModifyEmployees extends AbstractService
{
	public function update(int $id, array|null $request): void
	{
		$employee = $this->registry->getRepository(Employees::class)->find($id);
		if (!is_array($request)) {
			$this->code = Response::HTTP_BAD_REQUEST;
			return;
		}
		if (!$employee) {
			$this->create($request);
			return;
		}

		$employee = $this->setEmployeeData($request, $employee);
		$errors = $this->validator->validate($employee);
		if (count($errors) > 0) {
			$this->code = Response::HTTP_BAD_REQUEST;
			$this->setErrorResults($errors);
			return;
		}

		$this->setResults($employee);
		$this->code = Response::HTTP_OK;
	}

	public function create(array|null $request): void
	{
		if (!is_array($request)) {
			$this->code = Response::HTTP_BAD_REQUEST;
			return;
		}
		$employee = $this->setEmployeeData($request);
		$errors = $this->validator->validate($employee);
		if (count($errors) > 0) {
			$this->code = Response::HTTP_BAD_REQUEST;
			$this->setErrorResults($errors);
			return;
		}

		$this->setResults($employee);
		$this->code = Response::HTTP_OK;
	}

	public function setEmployeeData(array $request, Employees|null $employee = null): Employees
	{
		$employee = $employee ?? new Employees();
		!isset($request['firstName']) ?: $employee->setFirstName($request['firstName']);
		!isset($request['lastName']) ?: $employee->setLastName($request['lastName']);
		!isset($request['email']) ?: $employee->setEmail($request['email']);
		!isset($request['phoneNumber']) ?: $employee->setPhoneNumber($request['phoneNumber']);
		!isset($request['fkIdCompany']) ?: $employee->setFkIdCompany($request['fkIdCompany']);

		return $employee;
	}

	private function setResults(Employees $employee): void
	{
		(new EmployeesRepository($this->registry))->save($employee, true);
		$newEmployee = new GetEmployees($this->registry);
		$newEmployee->getById($employee->getId());

		$this->results = $newEmployee->getResults();
	}

	public function delete(int $id): void
	{
		$employee = $this->registry->getRepository(Employees::class)->find($id);
		if (!$employee) {
			$this->code = Response::HTTP_BAD_REQUEST;
			return;
		}

		(new EmployeesRepository($this->registry))->remove($employee, true);
		$this->code = Response::HTTP_OK;
	}
}