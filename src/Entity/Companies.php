<?php

namespace App\Entity;

use App\Repository\CompaniesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[UniqueEntity('nip', message: 'Passed nip is already in use!')]
#[ORM\Entity(repositoryClass: CompaniesRepository::class)]
class Companies
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column]
	private ?int $id = null;

	#[ORM\Column(length: 255)]
	#[Assert\NotBlank(message: 'companyName is required')]
	private ?string $companyName = null;

	#[ORM\Column(length: 255)]
	#[Assert\NotBlank(message: 'nip is required')]
	#[Assert\Length(
		min: 10,
		max: 10,
		minMessage: 'nip is to short',
		maxMessage: 'nip is to long'
	)]
	#[Assert\Type(type: 'numeric', message: 'nip should be numeric')]
	private ?string $nip = null;

	#[ORM\Column(length: 255)]
	#[Assert\NotBlank(message: 'street is required')]
	private ?string $street = null;

	#[ORM\Column(length: 255)]
	#[Assert\NotBlank(message: 'street is required')]
	private ?string $city = null;

	#[ORM\Column]
	#[Assert\NotBlank(message: 'buildingNumber is required')]
	private ?int $buildingNumber = null;

	#[ORM\Column(length: 255, nullable: true)]
	private ?string $flatNumber = null;

	#[ORM\Column(length: 6)]
	#[Assert\NotBlank(message: 'zipCode is required')]
	#[Assert\Length(
		min: 6,
		max: 6,
		minMessage: 'zipCode is to short',
		maxMessage: 'zipCode is to long'
	)]
	#[Assert\Regex(pattern: '/\d{2}-\d{3}/',message: 'Invalid zipCode pattern (00-000 required')]
	private ?string $zipCode = null;



	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCompanyName(): ?string
	{
		return $this->companyName;
	}

	public function setCompanyName(string $companyName): self
	{
		$this->companyName = $companyName;

		return $this;
	}

	public function getNip(): ?string
	{
		return $this->nip;
	}

	public function setNip(string $nip): self
	{
		$this->nip = $nip;

		return $this;
	}

	public function getStreet(): ?string
	{
		return $this->street;
	}

	public function setStreet(string $street): self
	{
		$this->street = $street;

		return $this;
	}

	public function getBuildingNumber(): ?int
	{
		return $this->buildingNumber;
	}

	public function setBuildingNumber(int $buildingNumber): self
	{
		$this->buildingNumber = $buildingNumber;

		return $this;
	}

	public function getFlatNumber(): ?string
	{
		return $this->flatNumber;
	}

	public function setFlatNumber(?string $flatNumber): self
	{
		$this->flatNumber = $flatNumber;

		return $this;
	}

	public function getZipCode(): ?string
	{
		return $this->zipCode;
	}

	public function setZipCode(string $zipCode): self
	{
		$this->zipCode = $zipCode;

		return $this;
	}

	public function getCity(): ?string
	{
		return $this->city;
	}

	public function setCity(?string $city): self
	{
		$this->city = $city;

		return $this;
	}
}
